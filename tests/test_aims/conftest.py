""" Pytest fixtures for AIMS tests
"""
import json
import hashlib
import os

import ase
import pytest
import shutil
from pathlib import Path
import numpy as np
from ase.build import bulk, molecule
from ase.calculators.aims import AimsTemplate


OUTPUT_FILES_DIR = Path(__file__).parent / "output_files"


@pytest.fixture(autouse=True)
def aims_species_dir():
    # Prevent aims tests from crashing in CI.
    species_dir = os.environ.get('AIMS_SPECIES_DIR')
    if species_dir is None:
        pytest.skip(reason='aims not configured')


def calculate_hash(atoms, parameters):
    def md5_hash(some_dict):
        """Hashing the inputs"""
        dhash = hashlib.md5()
        # We need to sort arguments so {'a': 1, 'b': 2} is
        # the same as {'b': 2, 'a': 1}
        encoded = json.dumps(some_dict, sort_keys=True).encode()
        dhash.update(encoded)
        return dhash.hexdigest()[:8]

    """Serialize atoms and parameters, get a hash"""
    for key in ("species_dir", "run_command"):
        if key in parameters:
            parameters.pop(key)
    atoms_dict = {
        k: v.tolist() if isinstance(v, np.ndarray) else v
        for k, v in atoms.todict().items()
    }
    return md5_hash({"atoms": atoms_dict, "parameters": parameters})


@pytest.fixture
def mock_execute(mocker):
    def copy_results(src, dest):
        """A mocked Aims calculate method. It calculates the hash of the inputs and copies over"""
        for output_file in (src.glob("*")):
            if output_file.is_file():
                shutil.copy(output_file, dest)

    def _mock_execute(atoms, parameters, directory):
        """Actual fixture"""
        inputs_hash = calculate_hash(atoms, parameters)
        if OUTPUT_FILES_DIR.exists() and inputs_hash in os.listdir(OUTPUT_FILES_DIR):
            mocked_calculator = mocker.patch.object(AimsTemplate, "execute")
            mocked_calculator.side_effect = lambda i, j: copy_results(
                OUTPUT_FILES_DIR / inputs_hash, directory
            )
    return _mock_execute


def cachable(cache_dir):
    # FIXME: work in progress
    def _wrapper(f):
        def _inner(*args, **kwargs):
            # find atoms, parameters and directory
            atoms = args[0] if isinstance(args[0], ase.Atoms) else args[0].atoms
            parameters, directory = args[1:3]
            inputs_hash = calculate_hash(atoms, parameters)
            if cache_dir.exists():
                f(*args, **kwargs)
        return _inner
    return _wrapper


@pytest.fixture
def crystal_gw():
    return bulk("Li")


@pytest.fixture
def atoms():
    return bulk("Si")


@pytest.fixture
def cluster():
    return molecule("C2H4")


@pytest.fixture
def parameters():
    return {
        "xc": "pw-lda",
        "k_grid": [4, 4, 4],
        "species_dir": os.environ.get("AIMS_SPECIES_DIR")
    }

