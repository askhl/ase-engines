""" A collection of tests for GW AIMS calculation
"""
import pytest

from engines.aims.aims import AimsEngine, AimsConfig
from .conftest import OUTPUT_FILES_DIR


def test_1d_2d(parameters, tmp_path):
    """Test against 1d and 2d structures"""
    from ase.build import nanotube, bcc100

    configuration = AimsConfig()
    engine = AimsEngine(configuration)

    with pytest.raises(ValueError):
        atoms = nanotube(3, 3)
        engine.gw(atoms, parameters, tmp_path)
    with pytest.raises(ValueError):
        atoms = bcc100("Li", [2, 2, 2])
        engine.gw(atoms, parameters, tmp_path)


def test_3d_error(crystal_gw, parameters, tmp_path):
    """Test bulk structure with wrong parameters"""
    parameters["qpe_calc"] = "gw"
    configuration = AimsConfig()
    engine = AimsEngine(configuration)

    with pytest.raises(ValueError):
        engine.gw(crystal_gw, parameters, tmp_path)
    parameters["qpe_calc"] = "gw_expt"
    with pytest.raises(KeyError):
        engine.gw(crystal_gw, parameters, tmp_path)


@pytest.mark.skipif(not OUTPUT_FILES_DIR.exists(), reason="No output data")
def test_3d(crystal_gw, parameters, tmp_path, mock_execute):
    """Test bulk structure"""
    configuration = AimsConfig()
    engine = AimsEngine(configuration)

    parameters["qpe_calc"] = "gw_expt"
    parameters["anacon_type"] = "two-pole"
    mock_execute(crystal_gw, parameters, tmp_path)

    calc = engine.gw(crystal_gw, parameters, tmp_path)
    eigenvalues = calc.read_results()["eigenvalues"]
    assert eigenvalues.shape == (64, 11, 1)
    assert eigenvalues[0, 0, 0] == -50.77318


def test_0d_error(atoms, cluster, parameters, tmp_path):
    """Test 0-D structure with wrong parameters"""
    configuration = AimsConfig()
    engine = AimsEngine(configuration)

    parameters.pop("k_grid")
    # no keyword
    with pytest.raises(KeyError):
        engine.gw(cluster, parameters, tmp_path)
    # wrong keyword
    parameters["qpe_calc"] = "gw_expt"
    with pytest.raises(ValueError):
        engine.gw(cluster, parameters, tmp_path)
    # right keyword, no anacon_type
    parameters["qpe_calc"] = "gw"
    with pytest.raises(KeyError):
        engine.gw(cluster, parameters, tmp_path)
    # right keyword, wrong anacon_type
    parameters["anacon_type"] = "something"
    with pytest.raises(KeyError):
        engine.gw(cluster, parameters, tmp_path)
    parameters["anacon_type"] = "two-pole"
    # bandpath for cluster calculations
    bandpath = atoms.cell.bandpath(density=10)
    with pytest.raises(ValueError):
        engine.gw(cluster, parameters, tmp_path, bandpath)


@pytest.mark.skipif(not OUTPUT_FILES_DIR.exists(), reason="No output data")
def test_0d(cluster, parameters, tmp_path, mock_execute):
    """Test 0-D structure"""
    configuration = AimsConfig()
    engine = AimsEngine(configuration)

    parameters.pop("k_grid")
    parameters["qpe_calc"] = "gw"
    parameters["anacon_type"] = "two-pole"
    mock_execute(cluster, parameters, tmp_path)

    calc = engine.gw(cluster, parameters, tmp_path)
    eigenvalues = calc.read_results()["eigenvalues"]
    assert eigenvalues.shape == (1, 48, 1)
    assert eigenvalues[0, 0, 0] == -266.18442
