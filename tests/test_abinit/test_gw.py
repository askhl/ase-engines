import numpy as np
from pathlib import Path

def test_gw_path(si_gw):
    expected_file = si_gw.directory / 'calco_GW'
    assert Path.exists(expected_file), f'Expected path {expected_file} does not exist.'

def test_gw_gap(si_gw):
    results = si_gw.read_results()
    np.testing.assert_allclose(results['qp_gap'][0, 0], 2.9725461, atol=1e-5), 'QP gap is not correct.'
