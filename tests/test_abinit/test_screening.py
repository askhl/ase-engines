import numpy as np
from pathlib import Path

def test_screening(si_screening, si_groundstate):
    # The screening calculation has to produce a SCR file: 
    expected_path = si_screening.directory / 'calco_SCR' # This name is hardcoded for now (From AbipyTemplate)
    assert Path.exists(expected_path), f'Expected path {expected_path} does not exist.'    

