import numpy as np
from pathlib import Path

def test_bandgap_files_shapes(si_bandgap, si_groundstate):
    bandgap_properties = si_bandgap.read_results()
    groundstate_properties = si_groundstate.read_results()

    assert bandgap_properties.get('nband', None) is not None
    assert bandgap_properties.get('eigenvalues', None) is not None
    assert bandgap_properties.get('ibz_kpoints', None) is not None

    # This is really just a ground-state calculation with more bands
    bg_eigenvaleus = bandgap_properties['eigenvalues']
    bg_nband = bandgap_properties['nband']

    assert bg_eigenvaleus.shape[2] == bg_nband, 'Number of bands in bandgap calculation does not match the number of bands in the eigenvalues array.'
    # We will want this calculation to produce a WFK file, so test that it exists. 
    expected_path = si_bandgap.directory / 'calco_WFK'
    assert Path.exists(expected_path), f'Expected path {expected_path} does not exist.'

def test_bandgap_eigenvalues(si_bandgap):
    # Specific test that calculation is correct:
    properties = si_bandgap.read_results()
    eigenvalues = properties['eigenvalues']
    np.testing.assert_allclose(eigenvalues[0, 0, 0], -3.05705509, atol=1e-5)
    np.testing.assert_allclose(eigenvalues[-1, -1, -1], 26.17388037, atol=1e-5)