from pathlib import Path
import shutil

from ase.build import bulk
from ase.calculators.espresso import (
    EspressoProfile, EspressoTemplate)


class EspressoCalculation:
    def __init__(self, atoms, parameters, directory):
        self.atoms = atoms
        self.parameters = parameters
        self.directory = directory

    def copy_pwscf_savedir(self, directory):
        name = 'pwscf.save'
        shutil.copytree(self.directory / name,
                        directory / name)

    def read_results(self):
        template = EspressoTemplate()
        return template.read_results(self.directory)


class Espresso:
    def __init__(self, profile):
        self.profile = profile
        self.template = EspressoTemplate()

    def groundstate(self, atoms, parameters, directory):
        directory.mkdir(exist_ok=True, parents=True)
        self.template.write_input(directory, atoms, parameters, ['energy'])
        self.template.execute(directory, self.profile)
        return EspressoCalculation(atoms, parameters, directory)

    def bandstructure(self, gs, bandpath, parameters, directory):
        # path.write('path.json')

        atoms = gs.atoms.copy()

        directory.mkdir(exist_ok=True, parents=True)

        try:
            gs.copy_pwscf_savedir(directory)
        except FileExistsError:
            pass  # XXX unsafe

        # print('efermi', efermi)

        bsparameters = {
            **gs.parameters,
            'kpts': bandpath,
            'fixdensity': True,
            **parameters}

        bsparameters['input_data']['control'].update(
            calculation='bands',
            restart_mode='restart',
            verbosity='high')

        self.template.write_input(directory, atoms, bsparameters, ['energy'])
        self.template.execute(directory, self.profile)
        return EspressoCalculation(atoms, bsparameters, directory)
