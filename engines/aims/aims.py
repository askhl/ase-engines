import os
import shutil
from pathlib import Path

import numpy as np

from ase.build import bulk
from ase.calculators.aims import AimsProfile, AimsTemplate
from ase.data import atomic_numbers
from ase.dft.kpoints import resolve_kpt_path_string, kpoint_convert
from ase.io.aims import read_aims_output


def ase2aims_bandpath(bandpath):
    """Covert and ASE Bandpath into an FHI-aims band structure input parameters format

    Following the implementation in aims-clims tool

    Parameters
    ----------
    bandpath: ase.dft.kpoints.BandPath
        The BandPath to move to FHI-aims

    Returns
    -------
    list
        The list of all band output parameters for FHI-aims
    """

    r_kpts = resolve_kpt_path_string(bandpath.path, bandpath.special_points)

    linesAndLabels = []
    k_index = -1

    for points, labels in zip(r_kpts[1], bandpath.path.split(",")):
        k_index += 1
        n_points = []
        for pt in points[1:]:
            k_start = k_index
            while np.any(bandpath.kpts[k_index] != pt):
                k_index += 1
            n_points.append(k_index - k_start + 1)

        linesAndLabels.append(
            [n_points, labels[:-1], labels[1:], points[:-1], points[1:]]
        )

    bands = []
    for segs in linesAndLabels:
        for points, lstart, lend, start, end in zip(*segs):
            bands.append(
                "band {:9.5f}{:9.5f}{:9.5f} {:9.5f}{:9.5f}{:9.5f} {:4} {:3}{:3}".format(
                    *start, *end, points, lstart, lend
                )
            )

    return bands


class AimsCalculation:
    def __init__(self, atoms, parameters, directory):
        """Representation of an FHI-aims calculation

        Parameters
        ----------
        atoms: ase.atoms.Atoms
            The atoms objects for the structure of a calculation
        parameters: dict
            The parameters for the calculation
        directory: str or Path
            The working directory of the calculation
        """

        self.atoms = atoms
        self.parameters = parameters
        self.directory = Path(directory)
        self.template = AimsTemplate()

    def copy_elsi_restart(self, directory, overwrite=False):
        """Copy ELSI restart data into a new directory

        Parameters
        ----------
        directory: str or Path
            The directory to copy files into
        overwrite: bool
            True if existing files can be overwritten
        """
        directory.mkdir(parents=True, exist_ok=overwrite)
        restart_filenames = self.directory.glob(f"*.csc")
        for file in restart_filenames:
            shutil.copy(file, directory)

    def read_results(self):
        """Wrapper to the read_results function on a template"""
        return self.template.read_results(self.directory)

    def read_aims_bands(self):
        """Read an FHI-aims bands output files into an ASE BandStructure"""
        band_files = sorted(self.directory.glob("*band*"))
        kpts = []
        eigenvalues = []
        occupations = []
        spins = []
        for band_file in band_files:
            spins.append(int(band_file.name.split("band")[-1][0]))
            lines = open(band_file, "r").readlines()
            for line in lines:
                split_line = line.split()
                k_pt = [float(kk) for kk in split_line[1:4]]
                if (len(kpts) == 0) or not np.allclose(k_pt, kpts[-1]):
                    kpts.append(k_pt)
                    eigenvalues.append([float(ev) for ev in split_line[5::2]])
                    occupations.append([float(oc) for oc in split_line[4::2]])
        eigenvalues = np.array(eigenvalues).reshape((len(set(spins)), len(kpts), -1))
        occupations = np.array(occupations).reshape((len(set(spins)), len(kpts), -1))
        return {
            "kpts": np.array(kpts),
            "eigenvalues": eigenvalues,
            "occupations": occupations,
        }


class AimsConfig:
    def __init__(self, argv=None, basis_set_direc=None):
        """Defines the configuration for an FHI-aims calculation

        Parameters
        ----------
        argv: str or list
            The run argument for FHI-aims
        basis_set_direc: str or Path
            The location of the FHI-aims species directory
        """
        self.profile = self.make_profile(argv)

        # Should be in engines configuration file
        if basis_set_direc is None:
            basis_set_direc = os.environ.get("AIMS_SPECIES_DIR")

        if basis_set_direc is None:
            raise ValueError(f"basis_set_direc not definable")

        self.base_species_direc = Path(basis_set_direc)

    def make_profile(self, argv=None):
        """Create an AimsProfile object from the run command

        Parameters
        ----------
        argv: str or list
            The list of command line arguments to run FHI-aims

        Returns
        -------
        AimsProfile
            The profile object to run FHI-aims
        """

        # Should be a part of the engines configuration file
        if argv is None:
            argv = os.getenv("ASE_AIMS_COMMAND", "aims.x").split()
        elif isinstance(argv, str):
            argv = argv.split()

        return AimsProfile(argv)

    def fetch_species(self, atoms, working_direc=Path.cwd(), species_parameters=None):
        """Set the basis set information for FHI-aims

        Parameters
        ----------
        atoms: ase.Atoms
            The atoms object for the structure
        working_direc: str or Path
            The working directory
        species_parameters: dict
            atom_species (str): basis_set type (str)

        Returns
        -------
        Path
            The directory to use as the species_dir in the calculator
        """
        working_direc = Path(working_direc)

        # Should be in engines configuration file
        if species_parameters is None:
            species_parameters = {}
        else:
            species_parameters = species_parameters.copy()

        # Should be configurable really bad to do this way
        if "default" not in species_parameters:
            if len(species_parameters) > 0:
                species_parameters["default"] = list(species_parameters.values())[0]
            else:
                species_parameters["default"] = "light"

        uniques_species = set(atoms.symbols)
        numbers = [atomic_numbers[symb] for symb in uniques_species]
        if len(set(species_parameters.values())) == 1:
            if all(
                [
                    (self.base_species_direc / f"{number:02}_{symb}_default").exists()
                    for symb, number in zip(uniques_species, numbers)
                ]
            ):
                return self.base_species_direc
            elif all(
                [
                    (
                        self.base_species_direc
                        / species_parameters["default"]
                        / f"{number:02}_{symb}_default"
                    ).exists()
                    for symb, number in zip(uniques_species, numbers)
                ]
            ):
                return self.base_species_direc / species_parameters["default"]
            else:
                raise FileExistsError(
                    "Basis set information for all species is not provided"
                )

        # Possible to make this configurable this is a temp direc
        basis_dir = working_direc / "basissets/"
        basis_dir.mkdir(exist_ok=True)

        for symb, number in zip(uniques_species, numbers):
            default_set = species_parameters.get(symb, species_parameters["default"])
            check_file_default_sets = (
                self.base_species_direc / default_set / f"{number:02}_{symb}_default"
            )
            if check_file_default_sets.exists():
                shutil.copy(check_file_default_sets, basis_dir)
                continue

            raise FileExistsError(
                f"Basis set information for the species {symb} is not provided in the available directory: {self.base_species_direc}."
            )
        return basis_dir


class AimsEngine:
    def __init__(self, configuration):
        """The engine that runs an FHI-aims calculation

        Parameters
        ----------
        configuration: AimsConfig
            The machine configuration class (can be replaced by AimsProfile in the future)
        """
        self.config = configuration
        self.template = AimsTemplate()

    def check_directory(self, directory, overwrite):
        """Checks if a directory is ready for a calculation

        Parameters
        ----------
        directory: str or Path
            The directory to check if it is able to run a calculation
        overwrite: bool
            If True then an old calculation can be overwritten

        Returns
        -------
        Path
            The Path object of the directory to run the calculation in

        Raises
        ------
        FileExistsError
            If the directory is not empty and overwrite is False
        """
        directory = Path(directory)
        directory.mkdir(exist_ok=True, parents=True)
        if not overwrite and (directory / "aims.out").exists():
            raise FileExistsError("Calculation already exists in the directory.")

        return directory

    def _add_elsi_restart(self, parameters):
        """Add ELSI restart info to the parameters (restarts a calculation)

        Parameters
        ----------
        parameters: dict
            The parameters for the calculation

        Returns
        -------
        dict
            The parameters with the updated ELSI restart information included.
        """
        parameters = parameters.copy()

        # TARP: Unless specified differently write the internal eigenstates (saved for ELSI restarts) to be read in for future calculations
        if "elsi_restart" not in parameters:
            parameters["elsi_restart"] = "read_and_write 1000"

        return parameters

    def _adjust_species(self, atoms, parameters, directory):
        """Adjusts the parameters to include the correct basis set information

        Parameters
        ----------
        atoms: ase.atoms.Atoms
            The atoms objects for the structure of a calculation
        parameters: dict
            The parameters for the calculation
        directory: str or Path
            The working directory of the calculation

        Returns
        -------
        dict
            The updated parameters dictionary with the correct basis set information
        """
        parameters = parameters.copy()
        parameters["species_dir"] = self.config.fetch_species(
            atoms, directory, parameters.get("species_parameters", None)
        )

        return parameters

    def _run_calculation(self, atoms, parameters, directory, properties):
        """Run an FHI-aims calculation

        Parameters
        ----------
        atoms: ase.atoms.Atoms
            The atoms objects for the structure of a calculation
        parameters: dict
            The parameters for the calculation
        directory: str or Path
            The working directory of the calculation
        properties: list
            The properties to calculate

        Returns
        -------
        AimsCalculation
            The calculation object for the calculation
        """
        parameters = self._adjust_species(atoms, parameters, directory)
        parameters = self._add_elsi_restart(parameters)

        self.template.write_input(directory, atoms, parameters, properties)
        self.template.execute(directory, self.config.profile)

        return AimsCalculation(atoms, parameters, directory)

    def groundstate(
        self, atoms, parameters, directory, properties=["energy"], overwrite=False
    ):
        """Run an FHI-aims ground state calculation

        Parameters
        ----------
        atoms: ase.atoms.Atoms
            The atoms objects for the structure of a calculation
        parameters: dict
            The parameters for the calculation
        directory: str or Path
            The working directory of the calculation
        properties: list
            The properties to calculate
        overwrite: bool
            If True then an old calculation can be overwritten

        Returns
        -------
        AimsCalculation
            The calculation object for the calculation
        """
        directory = self.check_directory(directory, overwrite)

        return self._run_calculation(atoms, parameters, directory, properties)

    def bandstructure_single_shot(
        self, atoms, parameters, directory, bandpath, overwrite=False
    ):
        """Run an FHI-aims band structure calculation

        Parameters
        ----------
        atoms: ase.atoms.Atoms
            The atoms objects for the structure of a calculation
        parameters: dict
            The parameters for the calculation
        directory: str or Path
            The working directory of the calculation
        properties: list
            The properties to calculate
        overwrite: bool
            If True then an old calculation can be overwritten

        Returns
        -------
        AimsCalculation
            The calculation object for the calculation
        """
        directory = self.check_directory(directory, overwrite)

        aims_bandpath = ase2aims_bandpath(bandpath)
        outputs = parameters.pop("output", list()) + aims_bandpath
        parameters["output"] = outputs

        return self._run_calculation(atoms, parameters, directory, ["energy"])

    def bandstructure(self, gs, parameters, directory, bandpath, overwrite=False):
        """Run an FHI-aims band structure calculation from a previous ground state calculation

        Parameters
        ----------
        gs: AimsCalculation
            The calculation object of the ground state calculation
        parameters: dict
            The parameters for the calculation
        directory: str or Path
            The working directory of the calculation
        properties: list
            The properties to calculate
        overwrite: bool
            If True then an old calculation can be overwritten

        Returns
        -------
        AimsCalculation
            The calculation object for the calculation
        """
        try:
            gs.copy_elsi_restart(directory)
        except FileExistsError:
            pass

        return self.bandstructure_single_shot(gs.atoms, parameters, directory, bandpath)

    def gw(self, atoms, parameters, directory, bandpath=None, overwrite=False):
        """A GW calculation for aims

        Args:
            atoms (ase.Atoms): a relaxed Atoms object to calculate GW for
            parameters (dict): general parameters for the calculation
            directory (:obj:`list` or :obj:`pathlib.Path`): a directory to put the calculation into
            bandpath (list): a band path to calculate the GS band structure for
            overwrite (boolean): a directory overwrite flag

        Returns:
            an AimsCalculation object
        """

        directory = self.check_directory(directory, overwrite)

        # checks
        if sum(atoms.pbc) not in (0, 3):
            raise ValueError(
                "GW calculation can be performed only on a bulk or cluster geometry"
            )

        if all(atoms.pbc):
            if "qpe_calc" not in parameters:
                parameters["qpe_calc"] = "gw_expt"
            elif parameters["qpe_calc"] != "gw_expt":
                raise ValueError(
                    "`qpe_calc` has to be `gw_expt` for periodic calculations"
                )
        else:
            if bandpath is not None:
                raise ValueError(
                    "The band structure for a cluster geometry is not defined"
                )
            if "qpe_calc" not in parameters:
                parameters["qpe_calc"] = "gw"
            elif ("sc_self_energy" not in parameters) and (
                parameters["qpe_calc"] not in ("gw", "ev_scgw", "ev_scgw0", "mp2")
            ):
                raise ValueError("`qpe_calc` is not correct for cluster calculation")

        if all(atoms.pbc):
            if bandpath is None:
                #  let the default k-grid density be 30
                bandpath = atoms.cell.bandpath(
                    density=parameters.get("k_grid_density", 30)
                )

            aims_bandpath = ase2aims_bandpath(bandpath)
            outputs = parameters.pop("output", list()) + aims_bandpath
            parameters["output"] = outputs

        if ("anacon_type" not in parameters) or (
            str(parameters["anacon_type"]) not in ("two-pole", "pade", "0", "1")
        ):
            raise KeyError(
                "GW calc should have `anacon_type` parameter with the value from [`two-pole`, `pade`]"
            )

        return self._run_calculation(atoms, parameters, directory, ["energy"])
