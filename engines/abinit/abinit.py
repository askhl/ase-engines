import numpy as np
from ase.calculators.genericfileio import CalculatorTemplate
from abipy.abilab import AbinitInput, Structure, MultiDataset
from abipy.abilab import abiopen
from ase.units import GPa
from ase.calculators.calculator import kpts2mp


class AbipyTemplate(CalculatorTemplate):
    def __init__(self):
        super().__init__(
            'abinit',
            ['energy', 'free_energy', 'forces', 'stress', 'magmoms'])

        self.name = 'calc'
        self.inputname = f'{self.name}.abi'
        self.outputname = f'{self.name}.abo'

    def write_input(self, abinit_input, directory):
        # Sanitize: some parts of the input are apparently not correct?
        illegal_phrases = ['indata_prefix', 'tmpdata_prefix', 'outdata_prefix']
        sanitized_input = ''
        for line in str(abinit_input).split('\n'):
            legal_line = True
            for illegal_phrase in illegal_phrases:
                if illegal_phrase in line:
                    legal_line = False
            if legal_line:
                sanitized_input += line + '\n'

        with open(directory / self.inputname, 'w') as f:
            print(sanitized_input, file=f)

    def execute(self, directory, profile):
        profile.run(directory, self.inputname, self.outputname)

    def read_results(self, directory):
        """
        """
        properties = dict()

        for read_func in [self.read_groundstate, self.read_gw, self.read_ddb]:
            properties.update(read_func(directory))

        return properties

    def read_groundstate(self, directory):
        """
        Read results from groundstate calculation.

        Will read:
            energy, eigenvalues, ibz_kpoints and fermi level.

        Parameters:
        -----------
        directory: str
            Directory containing abinit GSR file.

        Returns:
        --------
        dict
        """
        file_path = directory / f'{self.name}o_GSR.nc'

        if not file_path.exists():
            return {'groundstate': False}

        properties = {'groundstate': True}
        with abiopen(file_path) as gsr:
            properties['nband'] = gsr.nband
            properties['energy'] = gsr.energy
            properties['stress'] = gsr.cart_stress_tensor * GPa
            properties['forces'] = gsr.cart_forces.tolist()
            properties['eigenvalues'] = gsr.ebands.eigens
            properties['ibz_kpoints'] = gsr.ebands.kpoints.to_array()
            properties['fermi_level'] = float(gsr.ebands.fermie)
            properties['atoms'] = gsr.structure.to_ase_atoms()
        return properties

    def read_gw(self, directory):
        """
        Read results from gw calculation.

        Parameters:
        -----------
        directory: str
            Directory where 'SIGRES' file is located.
        """
        gw_file = directory / f'{self.name}o_SIGRES.nc'
        properties = {}
        if not gw_file.exists():
            properties['gw'] = False
            return properties

        with abiopen(gw_file) as gw:
            properties['qp_gap'] = gw.qpgaps
            properties['gw_kpoints'] = gw.gwkpoints.to_array()

        properties['gw'] = True
        return properties

    def read_ddb(self, directory):
        """
        Read results from dielectric response calculation

        Paramaters
        -----------
        directory: str
            Directory where the 'DDB' file is located.
        """
        # TODO DDB files can have a lot of different results, we should
        # be able to return all that that is find there, and we should
        # know what is not there
        ef_file = directory / f'{self.name}o_DS2_DDB'
        properties = {}
        if not ef_file.exists():
            properties["dielectric"] = False
            return properties

        with abiopen(ef_file) as ddb:
            if ddb.has_bec_terms() and ddb.has_epsinf_terms():
                diel, bec = ddb.anaget_epsinf_and_becs()
                properties['dielectric_tensor'] = np.array(diel.tolist())
                properties['born_effective_charges'] = bec.values
                properties["dielectric"] = True
        return properties


class AbipyCalculation:
    def __init__(self, atoms, parameters, directory):
        self.atoms = atoms
        self.parameters = parameters
        self.directory = directory.resolve()

    def read_results(self):
        template = AbipyTemplate()
        return template.read_results(self.directory)

    def get_file_path(self, file_name):
        return self.directory / file_name

    def get_input(self):
        crystal_structure_parameters = ['acell', 'rprim', 'ntypat', 'znucl',
                                        'typat', 'natom', 'xred']
        multi = []
        with abiopen(self.directory / 'calc.abi') as abinit_input_file:
            for input_dict in abinit_input_file.datasets:
                pseudos = input_dict.get('pseudos', None)[1:-1].split(',')

                pp_dirpath = input_dict.get('pp_dirpath', '')[1:-1]
                pp = [f"{pp_dirpath}/{pseudo.strip()}" for pseudo in pseudos]

                structure = abinit_input_file.structure

                # Crystal structure parametes have to be removed:
                # (because they are already set in the structure)
                for keys in crystal_structure_parameters:
                    input_dict.pop(keys, None)

                inp = AbinitInput(structure, pseudos=pp, abi_kwargs=input_dict)
                multi.append(inp)

        abinit_input = MultiDataset.from_inputs(multi)
        return abinit_input

class Abinit:
    def __init__(self, profile):
        self.profile = profile
        self.template = AbipyTemplate()

    def groundstate(self, atoms, parameters, pseudo_potentials,
                    directory, execute=True):
        """
        Parameters
        ----------
        atoms: ase.Atoms
            The atoms object to do the calculation for.
        parameters: dict
            Dictionary of parameters to use for the calculation.
            Any valid abinit keyword is allowed.
        pseudo_potentials: list
            List of pseudopotentials to use.
        directory: str
            Directory where the calculation is to be performed.
        execute: bool
            If True, execute the calculation.

        Returns
        -------
        AbipyCalculation
        """
        input_dict = dict()  # No additional default parameters.
        input_dict.update(parameters)

        abinit_input = AbinitInput(Structure.from_ase_atoms(atoms),
                                    pseudos=pseudo_potentials,
                                    abi_kwargs=input_dict)

        # Make directory, profile and template:
        directory.mkdir(exist_ok=True, parents=True)
        # Write and execute:
        if execute:
            self.template.write_input(abinit_input, directory)
            self.template.execute(directory, self.profile)
        return AbipyCalculation(atoms, parameters=parameters,
                                directory=directory)

    def non_selfconsistent(self, gs, parameters, directory, execute=True):
        """
        Parameters
        ----------
        gs: AbipyCalculation
            The groundstate calculation to use as a starting point
            for the non-selfconsistent calculation.
        parameters: dict
            Parameters for the non-selfconsistent calculation.
            Any other valid abinit input variable can be included,
            but can drastically alter what abinit does!
        directory: str
            Directory where the calculation is to be performed.
        execute: bool
            If True, execute the calculation.
            Mainly included for testing purposes.

        Returns
        -------
        AbipyCalculation
        """
        # This assumes the template name is 'calco'
        den_path = gs.directory / 'calco_DEN'
        abipy_input = gs.get_input()[0]

        # We do this to have a default value for tolwfr
        abipy_input.pop_tolerances()
        tolwfr = parameters.pop("tolwfr", 1e-22)
        input_dict = dict(tolwfr=tolwfr,
                          getden_filepath=den_path,
                          iscf=-2)
        input_dict.update(parameters)

        abipy_input.set_vars(**input_dict)

        # Make directory, profile and template:
        directory.mkdir(exist_ok=True, parents=True)
        # Write and execute:
        if execute:
            self.template.write_input(abipy_input, directory)
            self.template.execute(directory, self.profile)
        return AbipyCalculation(gs.atoms.copy(), parameters, directory)

    def bandstructure(self, gs, bandpath, parameters, directory, execute=True):
        """
        Parameters
        ----------
        gs: AbipyCalculation
            The groundstate calculation to use as a starting point
            for the bandstructure calculation.
        bandpath: ase.dft.kpoints.BandPath
            The bandpath to calculate the bandstructure along.
        parameters: dict
            Parameters for the bandstructure calculation.
            Any other valid abinit input variable can be included,
            but can drastically alter what abinit does!
        directory: str
            Directory where the calculation is to be performed.
        execute: bool
            If True, execute the calculation.
            Mainly included for testing purposes.

        Returns
        -------
        AbipyCalculation
        """
        input_dict = dict(kptopt=0,
                          kpt=bandpath.kpts,
                          nkpt=len(bandpath.kpts))
        input_dict.update(parameters)
        return self.non_selfconsistent(gs, input_dict, directory, execute)

    def dos(self, gs, kpts, parameters, directory, execute=True):
        """
        Parameters
        ----------
        gs: AbipyCalculation
            The groundstate calculation to use as a starting point
            for the bandstructure calculation.
        kpts: list
            The k-point grid for the DOS computation
        parameters: dict
            Parameters for the bandstructure calculation.
            Any other valid abinit input variable can be included,
            but can drastically alter what abinit does!
        directory: str
            Directory where the calculation is to be performed.
        execute: bool
            If True, execute the calculation.
            Mainly included for testing purposes.

        Returns
        -------
        AbipyCalculation
        """
        mp = kpts2mp(gs.atoms, kpts)

        input_dict = dict(kptopt=1,
                          ngkpt=mp,
                          nshiftk=1,
                          shiftk=(np.array(mp) + 1) % 2 * 0.5)
        input_dict.update(parameters)
        return self.non_selfconsistent(gs, input_dict, directory, execute)

    def bandgap(self, gs, parameters, directory, execute=True):
        """
        Parameters
        ----------
        gs: AbipyCalculation
            The groundstate calculation to use as a starting point
            for the bandgap calculation.
        parameters: dict
            Parameters for the bandgap calculation.
            Any other valid abinit input variable can be included,
            but can drastically alter what abinit does!
        directory: str
            Directory where the calculation is to be performed.
        execute: bool
            If True, execute the calculation.
            Mainly included for testing purposes.

        Returns
        -------
        AbipyCalculation
        """
        nband = gs.read_results()['nband']
        input_dict = {"nband": nband + 10}
        input_dict.update(parameters)
        return self.non_selfconsistent(gs, input_dict, directory, execute)

    def relaxation(self, atoms, parameters, pseudo_potentials,
                   directory, execute=True):
        """
        Parameters
        ----------
        atoms: ase.Atoms
            The atoms object to do the calculation for.
        parameters: dict
            Parameters for the relaxation calculation.
            Can contain any valid abinit input variable.
        pseudo_potentials: list
            List of full paths to the pseudos potentials to use.
        directory: str
            Directory where the calculation is to be performed.
        execute: bool
            If True, execute the calculation.
            Mainly included for testing purposes.
        """
        # TODO have default dilatmx/ecutsm according to optcell
        # Input dictionary:
        input_dict = dict(ionmov=2,
                          optcell=0,
                          ntime=100,
                          dilatmx=1.05,
                          ecutsm=0.5,
                          chksymtnons=0)

        input_dict.update(parameters)
        abinit_input = AbinitInput(Structure.from_ase_atoms(atoms),
                                   pseudos=pseudo_potentials,
                                   abi_kwargs=input_dict)

        # Make directory, profile and template:
        directory.mkdir(exist_ok=True, parents=True)
        # Write and execute:
        if execute:
            self.template.write_input(abinit_input, directory)
            self.template.execute(directory, self.profile)
        return AbipyCalculation(atoms, parameters=parameters,
                                directory=directory)

    def screening_ppmode(self, bg, parameters, directory, execute=True):
        """
        Parameters
        ----------
        bg: AbipyCalculation
            The bandgap calculation to use as a starting point
            for the screening calculation.
        atoms: ase.Atoms
            The atoms object to do the calculation for.
        parameters: dict
            Parameters for the screening calculation.
            Any valid abinit input variable can be included,
            but can drastically alter what abinit does!
        Directory:
            Directory where the calculation is to be performed.
        execute: bool
            If True, execute the calculation.
            Mainly included for testing purposes.
        """
        abinit_input = bg.get_input().deepcopy()[0]
        # This assumes the template name is 'calc'
        wfk_path = bg.directory / 'calco_WFK'
        """
        MG: FIXME. This should be an input parameter and
        propagated to the SIGMA part.
        Also, nband is missing and cannot be greater that
        nband stored in the WFK
        """
        ecuteps = parameters.pop("ecuteps", 2.0)
        input_dict = dict(optdriver=3,
                          ecuteps=ecuteps,
                          getden_filepath=None,
                          getwfk_filepath=wfk_path)

        input_dict.update(parameters)
        abinit_input.set_vars(**input_dict)

        # Make directory, profile and template:
        directory.mkdir(exist_ok=True, parents=True)
        # Write and execute:
        if execute:
            self.template.write_input(abinit_input, directory)
            self.template.execute(directory, self.profile)
        return AbipyCalculation(bg.atoms.copy(), parameters, directory)

    def gw(self, bg, scr, parameters, directory, execute=True):
        """
        Parameters
        ----------
        bg: AbipyCalculation
            The bandgap calculation to use as a starting point
            for the GW calculation.
        scr: AbipyCalculation
            The screening calculation to use as a starting point
            for the GW calculation.
        parameters: dict
            Parameters for the GW calculation. Any valid abinit input variable
            can be included, but can drastically alter what abinit does!
        directory: str
            Directory where the calculation is to be performed.
        """
        # Path stuff:
        scr_path = scr.directory / 'calco_SCR'
        wfk_path = bg.directory / 'calco_WFK'

        gw_input = scr.get_input().deepcopy()[0]
        # MG FIXME: Make sure that nband, ecuteps are in parameters.
        input_dict = dict(
                    optdriver=4,
                    ecutsigx=gw_input.get('ecut'),
                    getscr_filepath=scr_path,
                    getwfk_filepath=wfk_path)
        input_dict.update(parameters)
        gw_input.set_vars(input_dict)

        # Make directory, profile and template:
        directory.mkdir(exist_ok=True, parents=True)
        # Write and execute:
        if execute:
            self.template.write_input(gw_input, directory)
            self.template.execute(directory, self.profile)

        return AbipyCalculation(bg.atoms, parameters, directory)

    def dielectric_response(self, gs, parameters_ddk, parameters_dde,
                            directory, execute=True):
        """
        Parameters
        ----------
        gs: AbipyCalculation
            The groundstate calculation to use as a starting point
            for the response function calculation.
        parameters: dict
            Parameters for the response function calculation.
            Any other valid abinit input variable can be included,
            but can drastically alter what abinit does!
        directory: str
            Directory where the calculation is to be performed.
        execute: bool
            If True, execute the calculation.
            Mainly included for testing purposes.

        Returns
        -------
        AbipyCalculation
        """
        # Assume template name is 'calco'
        den_path = gs.directory / 'calco_DEN'
        wfk_path = gs.directory / 'calco_WFK'

        gs_input = gs.get_input().deepcopy()[0]
        gs_input.set_vars(dict(getden_filepath=den_path))
        gs_input.set_vars(dict(getwfk_filepath=wfk_path))
        gs_input.pop_irdvars()

        # TODO find a way to let abipy take care of the symmetries and
        # still have access to the correct ddk

        # Create input for the ddk
        tol_ddk = parameters_ddk.pop("tolerance", 1e-22)
        ddk_input = gs_input.deepcopy()
        ddk_input.pop_tolerances()
        ddk_input.set_vars(rfelfd=2,
                           rfdir=[1, 1, 1],
                           nqpt=1,
                           qpt=[0, 0, 0],
                           kptopt=2,
                           iscf=-3,
                           tolwfr=tol_ddk,
                           **parameters_ddk)

        # Create input for the dde
        tol_dde = parameters_dde.pop("tolerance", 1e-22)
        dde_input = gs_input.deepcopy()
        dde_input.pop_tolerances()
        dde_input.set_vars(rfelfd=3,
                           rfphon=1,
                           rfdir=[1, 1, 1],
                           rfatpol=[1, len(gs_input.structure)],
                           nqpt=1,
                           qpt=[0, 0, 0],
                           kptopt=2,
                           iscf=-3,
                           getddk=-1,
                           tolvrs=tol_dde,
                           **parameters_dde)

        multi = MultiDataset.from_inputs([ddk_input, dde_input])

        parameters = dict(parameters_ddk=parameters_ddk,
                          parameters_dde=parameters_dde)

        # Make directory, profile and template:
        directory.mkdir(exist_ok=True, parents=True)
        # Write and execute:
        if execute:
            self.template.write_input(multi, directory)
            self.template.execute(directory, self.profile)
        return AbipyCalculation(gs.atoms, parameters, directory)


if __name__ == '__main__':
    from ase.build import bulk
    from pathlib import Path
    from asetest import DataFiles
    from ase.calculators.abinit import AbinitProfile

    directory = Path('out_si/')
    atoms = bulk("AlN", "zincblende", 5.6)

    datafiles = DataFiles()
    abinit_path = datafiles.paths['abinit'][0]
    pseudo = [str(abinit_path / '13-Al.LDA.fhi'),
              str(abinit_path / '07-N.LDA.fhi')]

    profile = AbinitProfile(["abinit"])
    abi = Abinit(profile)

    parameters = {"ecut": 8,
                  "tolvrs": 1e-12,
                  "ngkpt": [1, 1, 1],
                  "chksymbreak": 0,
                  "diemac": 9}
    gs = abi.groundstate(atoms, parameters, pseudo, directory / 'gs')
    dos = abi.dos(gs, [1, 1, 1], {}, directory / 'dos')
    ef = abi.dielectric_response(gs, {}, {}, directory / 'ef')
    bg = abi.bandgap(gs, {}, directory / 'bg')
    scr = abi.screening_ppmode(bg, {}, directory / 'scr')
    gw = abi.gw(bg, scr, {}, directory / 'gw')
    print(ef.read_results())
